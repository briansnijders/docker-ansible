FROM python:3-alpine

# Install Alpine packages
RUN apk add build-base libffi-dev

# Install Python modules
RUN pip install pip --upgrade
RUN pip install ansible 

# Install Ansible plugins
RUN ansible-galaxy collection install community.general

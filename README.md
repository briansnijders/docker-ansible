Docker Ansible
==============

The Dockerfile in this repository packages Ansible in a generic multi-purpose Docker image to use Ansible in downstream jobs. Bundling Ansible in a Docker image and have GitLab use that Docker image as a base image, allows you to run Ansible tasks from GitLab easily.

In order to use the generated Docker image in a downstream job, include the following minimal configuration in the `.gitlab-ci.yml` of the downstream job:

```
deploy:
  image:
    name: registry.gitlab.com/briansnijders/docker-ansible:main
  stage: deploy
  script:
    - >-
      ansible-playbook <playbook.yml>
```
